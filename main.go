package main

import (
	"fmt"
	"mta-hosting-optimizer/config"
	"mta-hosting-optimizer/handler"
	"mta-hosting-optimizer/service"
	"net/http"
)

func main() {
	config.Init()

	ipConfigService := service.NewIpConfigService()
	serverDataService := service.NewServerDataService(ipConfigService)

	serverDataService.InitializeServerDataMap()

	http.HandleFunc("/mta-hosting-optimizer", handler.MtaHostingOptimizerHandler(serverDataService))

	port := 8080
	fmt.Printf("Server is listening on port %d\n", port)
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		fmt.Println("Error starting the server:", err)
	}
}
