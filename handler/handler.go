package handler

import (
	"encoding/json"
	"mta-hosting-optimizer/service"
	"net/http"
)

func MtaHostingOptimizerHandler(serverDataService service.ServerDataService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		result := serverDataService.GetInefficientHostNames()

		OutputJSON(w, http.StatusOK, result)
	}
}

func OutputJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
