package handler

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

type mockServerDataService struct{}

func (m *mockServerDataService) GetInefficientHostNames() []string {
	return []string{"mta-prod-1", "mta-prod-3"}
}

func (m *mockServerDataService) InitializeServerDataMap() {
}

func TestIntegrationMtaHostingOptimizerHandler(t *testing.T) {
	serverDataService := &mockServerDataService{}
	handler := MtaHostingOptimizerHandler(serverDataService)

	req, err := http.NewRequest("GET", "/mta-hosting-optimizer", nil)
	assert.NoError(t, err)

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	assert.Equal(t, http.StatusOK, rr.Code)

	expectedResponse := `["mta-prod-1","mta-prod-3"]`
	assert.Equal(t, expectedResponse, rr.Body.String())
}

func TestOutputJSON(t *testing.T) {
	t.Run("Error in json.Marshal", func(t *testing.T) {
		w := httptest.NewRecorder()

		payload := make(chan int)

		OutputJSON(w, http.StatusOK, payload)

		assert.Equal(t, http.StatusInternalServerError, w.Code)
		assert.Equal(t, "Internal Server Error\n", w.Body.String())
	})
}
