package service

import (
	"fmt"
	"mta-hosting-optimizer/config"
	"mta-hosting-optimizer/models"
	"sync"
)

type IpConfigService interface {
	GetIpConfigData() ([]models.IpConfig, error)
}

type ServerDataService interface {
	GetInefficientHostNames() []string
	InitializeServerDataMap()
}

type ipConfigServiceImpl struct{}

func (i *ipConfigServiceImpl) GetIpConfigData() ([]models.IpConfig, error) {
	return []models.IpConfig{
		{"127.0.0.1", "mta-prod-1", true}, {"127.0.0.2", "mta-prod-1", false},
		{"127.0.0.3", "mta-prod-2", true}, {"127.0.0.4", "mta-prod-2", true},
		{"127.0.0.5", "mta-prod-2", false}, {"127.0.0.6", "mta-prod-3", false},
	}, nil
}

type serverDataServiceImpl struct {
	ipConfigService IpConfigService
	serverDataMap   map[string]models.ServerData
	mutex           sync.Mutex
}

func NewServerDataService(ipConfigService IpConfigService) ServerDataService {
	return &serverDataServiceImpl{
		ipConfigService: ipConfigService,
		serverDataMap:   make(map[string]models.ServerData),
	}
}

func (s *serverDataServiceImpl) InitializeServerDataMap() {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	ipConfigData, err := s.ipConfigService.GetIpConfigData()
	if err != nil {
		fmt.Println("Unable to get IpConfigData. Err:", err)
		return
	}

	for _, ipConfig := range ipConfigData {
		serverData, exists := s.serverDataMap[ipConfig.Hostname]
		if !exists {
			serverData = models.ServerData{Hostname: ipConfig.Hostname}
		}

		if ipConfig.Active {
			serverData.ActiveCount++
		}

		s.serverDataMap[ipConfig.Hostname] = serverData
	}
}

func (s *serverDataServiceImpl) GetInefficientHostNames() []string {
	var inefficientHostnames []string

	s.mutex.Lock()
	defer s.mutex.Unlock()

	threshold := config.GetThreshold()

	for hostname, data := range s.serverDataMap {
		if data.ActiveCount <= threshold {
			inefficientHostnames = append(inefficientHostnames, hostname)
		}
	}

	return inefficientHostnames
}

func NewIpConfigService() IpConfigService {
	return &ipConfigServiceImpl{}
}
