package service

import (
	"errors"
	"mta-hosting-optimizer/config"
	"mta-hosting-optimizer/models"
	"testing"

	"github.com/stretchr/testify/assert"
)

type mockIpConfigService struct{}

func (m *mockIpConfigService) GetIpConfigData() ([]models.IpConfig, error) {
	return []models.IpConfig{
		{"127.0.0.1", "mta-prod-1", true}, {"127.0.0.2", "mta-prod-1", false},
		{"127.0.0.3", "mta-prod-2", true}, {"127.0.0.4", "mta-prod-2", true},
		{"127.0.0.5", "mta-prod-2", false}, {"127.0.0.6", "mta-prod-3", false},
	}, nil
}

type mockErrorIpConfigService struct{}

func (m *mockErrorIpConfigService) GetIpConfigData() ([]models.IpConfig, error) {
	return nil, errors.New("mock error getting IpConfigData")
}

func TestServerDataInitialization(t *testing.T) {
	ipConfigService := &mockIpConfigService{}
	serverDataService := NewServerDataService(ipConfigService)
	serverDataService.InitializeServerDataMap()

	assert.Equal(t, 3, len(serverDataService.(*serverDataServiceImpl).serverDataMap))

	ipConfigServiceWithError := &mockErrorIpConfigService{}
	serverDataServiceWithError := NewServerDataService(ipConfigServiceWithError)
	serverDataServiceWithError.InitializeServerDataMap()

	assert.Equal(t, 0, len(serverDataServiceWithError.(*serverDataServiceImpl).serverDataMap))
}

func TestGetInefficientHostnames(t *testing.T) {
	ipConfigService := &mockIpConfigService{}
	serverDataService := NewServerDataService(ipConfigService)
	serverDataService.InitializeServerDataMap()

	config.SetThreshold(1)

	inefficientHostnamesCustomThreshold := serverDataService.GetInefficientHostNames()
	assert.ElementsMatch(t, []string{"mta-prod-1", "mta-prod-3"}, inefficientHostnamesCustomThreshold)
}
