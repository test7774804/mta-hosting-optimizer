package config

import (
	"fmt"
	"os"
	"strconv"
	"sync"
)

var (
	threshold int
	once      sync.Once
)

func Init() {
	once.Do(func() {
		threshold = getThreshold()
	})
}

func getThreshold() int {
	thresholdStr := os.Getenv("THRESHOLD")
	if thresholdStr == "" {
		thresholdStr = "1"
	}

	threshold, err := strconv.Atoi(thresholdStr)
	if err != nil {
		fmt.Println("Error converting threshold to integer:", err)
		threshold = 1
	}

	return threshold
}

func GetThreshold() int {
	return threshold
}

func SetThreshold(value int) {
	threshold = value
}
