package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetThreshold(t *testing.T) {
	Init()
	assert.Equal(t, 1, GetThreshold())
}

func TestSetThreshold(t *testing.T) {
	SetThreshold(42)
	assert.Equal(t, 42, GetThreshold())
}
