package models

type IpConfig struct {
	IP       string `json:"ip"`
	Hostname string `json:"hostname"`
	Active   bool   `json:"active"`
}

type ServerData struct {
	Hostname    string
	ActiveCount int
}
